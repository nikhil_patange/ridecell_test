
## Project Description

Assume that we are building a street parking spot reservation service. Each parking spot is identified by  its location (lat, lng). Users should be able to view street parking spots, reserve and pay for the parking  spots or cancel their reservations. Build REST API's for the following and hare the github repository  with us. You can populate your database with any dummy data you want.

## Requirements
• See available parking spots on a map    
• Search for an address and find nearby parking spot. (input: lat, lng, radius in meters. Output - list of  parking spots within the radius).  
• Reserve a parking spot  
• View existing reservations  
• Cancel an existing reservation  
• Show the user the cost of the reservation

## Running the Solution

 1. Project Structure

		 -- main.py :  Execute this for running flask server.
	 	 -- custom_functions.py : This contains relavant functions to support main.py
	 	 -- parking_spaces.db : An Sqlite3 database file.
	 	 -- requirements.txt  : Python dependencies.

 2. Requirements
	Dependencies can be found in `requirement.txt` file in project directory.
	
	Also install Sqlite3 drivers and add it to path, by following instruction from link below,
	
[https://www.sqlitetutorial.net/download-install-sqlite/](https://www.sqlitetutorial.net/download-install-sqlite/)

Use following to install dependencies:

`pip install -r requirements.txt`

## Solution API's

**1. See available parking spots on a map :**

API : http://127.0.0.1:5000//get_available_spots

Method : GET

Parameters : None

Sample Request:

`curl -X GET http://127.0.0.1:5000//get_available_spots`

Sample Response:
```json
[
    {
        "available": 1,
        "lat": 73.815951,
        "long": 18.603861,
        "per_hour_rate": 50,
        "spot_id": 1
    },
]
```
------------

**2. Search for an address and find nearby parking spot. (input: lat, lng, radius in meters. Output - list of  parking spots within the radius).**

API: http://127.0.0.1:5000/search_praking_spot

Method  : *GET*

Parameters:
*{"lat":Decimal,
"long":Decimal,
"radius":Decimal}*

Sample Request:

    curl -X GET \
      'http://127.0.0.1:5000/search_praking_spot?lat=73.81&long=18.48&radius=1' 
      
Sample Response :

    {
        "message": "2 records retrieved !",
        "result": [
            {
                "available": 1,
                "lat": 73.812772,
                "long": 18.480667,
                "per_hour_rate": 50,
                "spot_id": 14
            },
            {
                "available": 1,
                "lat": 73.816031,
                "long": 18.503351,
                "per_hour_rate": 50,
                "spot_id": 19
            }
        ]
    }



------------

**3. Reserve a parking spot :**

API: *http://127.0.0.1:5000/reserve_parking_spot*

Method  : *POST*

Parameters:
*{spot_id:Integer
hours:Decimal
customer_id:Integer}*

Sample Request:

    curl -X POST http://127.0.0.1:5000/reserve_parking_spot \
      -H 'content-type: multipart/form-data;
      -F spot_id=7 \
      -F hours=12 \
      -F customer_id=8

Sample Response :

    {
        "message": "Parking reservation for customer_id : 8, confirmed!",
        "result": [
            {
                "customer_id": 8,
                "date_timestamp": "2020-01-19 21:33:22.367667",
                "duration_in_hours": 12,
                "parking_spot_id": 7,
                "reservation_id": 13,
                "total_paid": 600
            }
        ]
    }



------------
**4. View existing reservations**

API: http://127.0.0.1:5000/view_reservations

Method  : *GET*

Parameters:
{reservation_id:Integer}

Sample Request:

    curl -X GET  'http://127.0.0.1:5000/view_reservations?reservation_id=10'
Sample Response:

    {
        "message": "Reservation found!",
        "result": [
            {
                "customer_id": 7,
                "date_timestamp": "2020-01-19 20:33:10.937374",
                "duration_in_hours": 4,
                "parking_spot_id": 4,
                "reservation_id": 10,
                "total_paid": 200
            }
        ]
    }

 **5. Cancel an existing reservation :**

 API : http://127.0.0.1:5000/cancel_reservation/<reservation_id:Integer>

 Method : *DELETE*

 Parameters : 
 *{}*

Sample Request:

    curl -X DELETE http://127.0.0.1:5000/cancel_reservation/8
Sample Response:

    {
        "message": "Reservation cancelled successfuly!",
        "result": [
            {
                "customer_id": 7,
                "date_timestamp": "2020-01-19 16:01:39.030108",
                "duration_in_hours": 2,
                "parking_spot_id": 2,
                "reservation_id": 8,
                "total_paid": 100
            }
        ]
    }

**6. Show the user the cost of the reservation:**

API: http://127.0.0.1:5000/get_reservation_cost

Method  : *GET*

Parameters:
{reservation_id:Integer}

Sample Request:

    curl -X GET 'http://127.0.0.1:5000/get_reservation_cost?reservation_id=10'
Sample Response:

    {
        "message": "Retrieved cost successfuly!",
        "result": [
            {
                "reservation_id": 10,
                "total_cost": 200
            }
        ]
    }
