import math
import re
import sqlite3
from math import cos, asin

from flask import g

DATABASE = 'parking_spaces.db'


def make_dicts(cursor, row):
    return dict((cursor.description[idx][0], value)
                for idx, value in enumerate(row))


def query_db(query, args=(), one=False):
    db = get_db()
    cur = db.execute(query, args)

    if re.match("insert|update|delete", query):
        db.commit()

    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)

    db.row_factory = make_dicts
    return db


def rad2deg(rad):
    return math.degrees(rad)


def deg2rad(deg):
    return math.radians(deg)


def get_bounding_circle(lat, long, radius, R=6371):
    maxLat = lat + rad2deg(radius / R)
    minLat = lat - rad2deg(radius / R)
    maxLon = long + rad2deg(asin(radius / R) / cos(deg2rad(lat)))
    minLon = long - rad2deg(asin(radius / R) / cos(deg2rad(lat)))
    _query = "select * from parking_spots where  lat Between " + str(minLat) + " And " + str(maxLat) + " " \
                                                                                                       "And long Between " + str(
        minLon) + " And " + str(maxLon)

    return _query
