import datetime
from flask import Flask, jsonify, request
from custom_functions import *

app = Flask(__name__)
DATABASE = 'parking_spaces.db'

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.route("/")
@app.route("/get_available_spots",methods=["GET"])
def get_spots_available():
    return jsonify(query_db("select * from parking_spots where available=1"))

@app.route("/search_praking_spot", methods=["GET"])
def search_nearby_parking_spot():
    try:
        lat = float(request.args.get("lat"))
        long = float(request.args.get("long"))
        radius = float(request.args.get("radius"))
        query = get_bounding_circle(lat, long, radius)
        resp = query_db(query)
        resp = jsonify({"result": resp,
                        "message": "{} records retrieved !".format(len(resp))})
        resp.status_code = 200
    except:
        resp = jsonify({"result": [],
                        "message": "Error processing request!"})
        resp.status_code = 500

    return resp

@app.route("/reserve_parking_spot", methods=["POST"])
def reserve_parking_spot():
    # Validate parameters
    try:
        parking_spot_id = int(request.form.get("spot_id"))
        parking_spot_hours = float(request.form.get("hours"))
        customer_id = int(request.form.get("customer_id"))
    except:
        resp = jsonify({"result": [],
                        "message": "Provide valid request parameters!"})
        resp.status_code = 200
        return resp

    # check if spot is available
    resp = query_db("select * from parking_spots where spot_id={} and available = 1".format(parking_spot_id))

    # if available confirm else return error message to user
    if resp != []:
        try:
            per_hour_rate = resp[0]["per_hour_rate"]

            query_db("update parking_spots set available = 0 where spot_id = {}".format(parking_spot_id))

            query_db("insert into parking_reservation (parking_spot_id ,customer_id, date_timestamp ,"
                     "duration_in_hours,total_paid) values (?,?,?,?,?)"
                     , (parking_spot_id, customer_id, datetime.datetime.now(),
                        parking_spot_hours, parking_spot_hours * per_hour_rate))

            # get and return newly added wntry in response for details on update
            new_entry = query_db('SELECT last_insert_rowid()')
            parking_reservation_id = new_entry[0]['last_insert_rowid()']
            new_entry = query_db(
                "select * from parking_reservation where reservation_id= {}".format(parking_reservation_id))

        except:
            return jsonify({"result": [],
                            "message": "Booking failed! Please try again later."})
    else:
        return jsonify({"result": resp,
                        "message": "Parking spot with id {} is unavailable.".format(parking_spot_id)
                        })

    return jsonify({"result": new_entry,
                    "message": "Parking reservation for customer_id : {}, confirmed!".format(customer_id)})


@app.route("/view_reservations", methods=["GET"])
def view_reservations():
    # Validate input parameters
    try:
        reservation_id = int(request.args.get("reservation_id"))
    except:
        # If invalid or no reservation id is provided
        return jsonify({"result": [],
                        "message": "Please provide valid reservation_id"})

    if reservation_id == None:
        resp = query_db("select * from parking_reservation")
        return jsonify({"result": resp,
                        "message": "Total {} active reservations found!".format(len(resp))})

    # Query and fetch reservation ID if not found respond with message.
    resp = query_db("select * from parking_reservation where reservation_id = {}".format(reservation_id))
    if resp != []:
        return jsonify({"result": resp,
                        "message": "Reservation found!"})
    else:
        return jsonify({"result": resp,
                        "message": "No reservation found against id : {}".format(reservation_id)})


@app.route("/cancel_reservation/<int:reservation_id>", methods=["DELETE"])
def cancel_reservation(reservation_id):
    # fetch row with given reservation ID, parse for parsking spot ID
    resp = query_db("select * from parking_reservation where reservation_id={}".format(reservation_id))
    if resp != []:
        parking_spot_id = resp[0]["parking_spot_id"]

        # Delete entry from parking reservation table
        query_db("delete from parking_reservation where reservation_id = {}".format(reservation_id))
        # update entry from parking_slot table
        query_db("update parking_spots set available = 1 where spot_id = {}".format(parking_spot_id))
        resp = jsonify({"result": resp,
                        "message": "Reservation cancelled successfuly!"})
        resp.status_code = 200
        return resp

    else:
        resp = jsonify({"result": resp,
                        "message": "Could not find reservation for id : {}".format(reservation_id)})
        resp.status_code = 200
        return resp


@app.route("/get_reservation_cost", methods=["GET"])
def get_reservation():
    # Validate input parameter
    try:
        reservation_id = int(request.args.get("reservation_id"))
    except:
        reservation_id = ""
    if reservation_id == "" or reservation_id == None:
        return jsonify({"result": [],
                        "message": "Please provide valid reservation ID"})

    resp = query_db("select * from parking_reservation where reservation_id={}".format(reservation_id))

    if resp != []:
        cost = resp[0]["total_paid"]
        resp = jsonify({"result": [{"reservation_id": reservation_id,
                                    "total_cost": cost}],
                        "message": "Retrieved cost successfuly!"})
        resp.status_code = 200
        return resp
    else:
        resp = jsonify({"result": [],
                        "message": "Could not find required resource!"})
        resp.status_code = 200
        return resp


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp


if __name__ == "__main__":
    app.run("127.0.0.1", port=5000, debug=True)
